from flask import Flask,render_template,request
import random

app = Flask(__name__)

@app.route('/dashboard')
def hello_world():
 	return render_template('index.html') 
#Hello

@app.route('/send')
def get_req():
	u = random.random() * 10000
	data = request.args["data"]
	dat = open(str(u)+'.txt','w')
	dat.write(data)
	dat.close()
	return "success"

if __name__=='__main__':
   app.run(host='0.0.0.0')

